Get-Mailbox -ResultSize Unlimited | 
Import-Module ExchangeOnlineManagement
Connect-ExchangeOnline -UserPrincipalName username@domain.ext

 
$mailboxes = Get-Mailbox -ResultSize Unlimited
$Language = "nl-NL"
$TimeZone = "W. Europe Standard Time"
 
foreach ($mailbox in $mailboxes) {

    # Skip System mailbox
    if (($mailbox -like 'Discovery Search Mailbox')) {
        continue
    }

    # Set Language, TimeZone and LocalizeDefaultFolderName
    Set-MailboxRegionalConfiguration -Identity $mailbox.SamAccountName -Language $Language -TimeZone $TimeZone -LocalizeDefaultFolderName

    Write-Host "Updating Language, TimeZone and LocalizeDefaultFolderName for $mailbox." -ForegroundColor Green
}

Disconnect-ExchangeOnline -Confirm:$false
