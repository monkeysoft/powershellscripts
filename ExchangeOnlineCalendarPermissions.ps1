Import-Module ExchangeOnlineManagement
Connect-ExchangeOnline -UserPrincipalName username@domain.ext

 
$mailboxes = Get-Mailbox -ResultSize Unlimited
$AccessRights = "PublishingEditor"
 
foreach ($mailbox in $mailboxes) {
    # Skip System mailbox and CEO
    if (($mailbox -like 'Discovery Search Mailbox') -or ($mailbox -like 'CEO Name')) {
        continue
    }

    # Search Calendar name and set Calendar rights to $AccessRights 
    $calendar = (($mailbox.SamAccountName)+ ":\" + (Get-MailboxFolderStatistics -Identity $mailbox.SamAccountName -FolderScope Calendar | Select-Object -First 1).Name)
    if (((Get-MailboxFolderPermission $calendar  | Where-Object {$_.User -like "Default"}).AccessRights) -notLike $AccessRights ) {
        Write-Host "Updating calendar $calendar permission for $mailbox." -ForegroundColor Green
        Set-MailboxFolderPermission -User "Default" -AccessRights $AccessRights -Identity $calendar
    }
    
#    # Give CEO extra rights to calendar
#    if (((Get-MailboxFolderPermission $calendar  | Where-Object {$_.User -like "CEO Name"}).AccessRights) -eq $null) {
#        Write-Host "Creating calendar $calendar permission for $mailbox." -ForegroundColor Yellow
#        Add-MailboxFolderPermission -User "CEO Username" -AccessRights "Owner" -Identity $calendar
#   }
#   Elseif (((Get-MailboxFolderPermission $calendar  | Where-Object {$_.User -like "CEO Name"}).AccessRights) -notLike "Owner" ) {
#       Write-Host "Updating calendar $calendar permission for $mailbox." -ForegroundColor Yellow
#       Set-MailboxFolderPermission -User "CEO Username" -AccessRights "Owner" -Identity $calendar
#    }
}

Disconnect-ExchangeOnline -Confirm:$false
